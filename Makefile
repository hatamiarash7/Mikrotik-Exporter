include .env
include .version

SHORTSHA=`git rev-parse --short HEAD`

LDFLAGS=-s -w
LDFLAGS+=-X main.appVersion=${VERSION}
LDFLAGS+=-X main.shortSha=$(SHORTSHA)

info: ## Show info about the project
	@echo Version = ${VERSION}
	@echo Repository = ${REPONAME}
	@echo Username = ${DOCKER_USERNAME}
	@echo Docker Token = ${DOCKER_TOKEN}
	@echo LDFLAGS = $(LDFLAGS)

clean: ## Clean the project
	@rm -rf dist/*

dev: clean ## Build the staging binaries
	go build -ldflags "$(LDFLAGS)" -o "dist/mikrotik-exporter" .

utils: ## Install required tools
	go install github.com/mitchellh/gox@latest

build: clean utils ## Build the production binaries
	CGO_ENABLED=0 gox -os="linux" -arch="amd64 arm64" -parallel=4 -ldflags "$(LDFLAGS)" -output "dist/mikrotik-exporter_{{.OS}}_{{.Arch}}"

docker: build ## Build & push Docker image
	@echo ${DOCKER_TOKEN} | docker login --username ${DOCKER_USERNAME} --password-stdin
	docker buildx create --name mybuilder --use
	docker buildx build -t ${DOCKER_USERNAME}/${REPONAME}:${VERSION} -t ${DOCKER_USERNAME}/${REPONAME}:latest --platform=linux/arm64,linux/amd64 . --push

docker-dev: build ## Build Docker image 
	docker buildx build -t ${DOCKER_USERNAME}/${REPONAME}:${VERSION} . --load

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: info build utils docker deploy help