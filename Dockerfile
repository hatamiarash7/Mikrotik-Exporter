FROM --platform=${BUILDPLATFORM:-linux/amd64} alpine:latest

ARG TARGETOS
ARG TARGETARCH

COPY dist/mikrotik-exporter_${TARGETOS}_${TARGETARCH} /app/mikrotik-exporter

COPY scripts/start.sh /app/

EXPOSE 9436

ENTRYPOINT ["/app/start.sh"]
